spec:
  inputs:
    stage:
      default: test
    image_prefix:
      default: "$CI_TEMPLATE_REGISTRY_HOST/security-products"
    image_tag:
      default: '5'
    image_suffix:
      default: ""
    excluded_analyzers:
      default: ""
    excluded_paths:
      default: "spec, test, tests, tmp"
    search_max_depth:
      default: '4'
    run_kubesec_sast:
      default: 'false'
    run_advanced_sast:
      default: false
      type: boolean
    include_experimental:
      default: 'false'
---
.sast-analyzer:
  stage: $[[ inputs.stage ]]
  allow_failure: true
  # these variables are used by the analyzer
  # TODO: propagate inputs when breaking down into separate components
  # E.g. SEARCH_MAX_DEPTH is overridden in some analyzers. We should pass the input instead.
  variables:
    SEARCH_MAX_DEPTH: $[[ inputs.search_max_depth ]]
    DEFAULT_SAST_EXCLUDED_PATHS: $[[ inputs.excluded_paths ]]
    SAST_EXCLUDED_PATHS: "$DEFAULT_SAST_EXCLUDED_PATHS"
  script:
    - /analyzer run
  artifacts:
    access: 'developer'
    reports:
      sast: gl-sast-report.json

.deprecated-16.8:
  extends: .sast-analyzer
  script:
    - echo "This job was deprecated in GitLab 16.8 and removed in GitLab 17.0"
    - echo "For more information see https://gitlab.com/gitlab-org/gitlab/-/issues/425085"
    - exit 1
  rules:
    - when: never

gitlab-advanced-sast:
  extends: .sast-analyzer
  image:
    name: "$[[ inputs.image_prefix ]]/gitlab-advanced-sast:${SAST_ANALYZER_IMAGE_TAG}$[[ inputs.image_suffix ]]"
  variables:
    SAST_ANALYZER_IMAGE_TAG: 1
    SEARCH_MAX_DEPTH: 20
  rules:
    - if: '"$[[ inputs.excluded_analyzers ]]" =~ /gitlab-advanced-sast/'
      when: never
    - if: '"$[[ inputs.run_advanced_sast ]]" != "true"'
      when: never
    - if: $CI_COMMIT_BRANCH &&
          $GITLAB_FEATURES =~ /\bsast_advanced\b/
      exists:
        - '**/*.py'
        - '**/*.go'
        - '**/*.java'
        - '**/*.jsp'
        - '**/*.js'
        - '**/*.jsx'
        - '**/*.ts'
        - '**/*.tsx'
        - '**/*.cjs'
        - '**/*.mjs'
        - '**/*.cs'
        - '**/*.rb'

brakeman-sast:
  extends: .deprecated-16.8

flawfinder-sast:
  extends: .deprecated-16.8

kubesec-sast:
  extends: .sast-analyzer
  image:
    name: "$[[ inputs.image_prefix ]]/kubesec:$[[ inputs.image_tag ]]"
  rules:
    - if: '"$[[ inputs.excluded_analyzers ]]" =~ /kubesec/'
      when: never
    - if: '$CI_COMMIT_BRANCH && "$[[ inputs.run_kubesec_sast ]]" == "true"'

mobsf-android-sast:
  extends: .deprecated-16.8

mobsf-ios-sast:
  extends: .deprecated-16.8

nodejs-scan-sast:
  extends: .deprecated-16.8

phpcs-security-audit-sast:
  extends: .deprecated-16.8

pmd-apex-sast:
  extends: .sast-analyzer
  image:
    name: "$[[ inputs.image_prefix ]]/pmd-apex:$[[ inputs.image_tag ]]"
  rules:
    - if: '"$[[ inputs.excluded_analyzers ]]" =~ /pmd-apex/'
      when: never
    - if: $CI_COMMIT_BRANCH
      exists:
        - '**/*.cls'

security-code-scan-sast:
  extends: .sast-analyzer
  script:
    - echo "This job was deprecated in GitLab 15.9 and removed in GitLab 16.0"
    - echo "For more information see https://gitlab.com/gitlab-org/gitlab/-/issues/390416"
    - exit 1
  rules:
    - when: never

semgrep-sast:
  extends: .sast-analyzer
  image:
    name: "$[[ inputs.image_prefix ]]/semgrep:$[[ inputs.image_tag ]]$[[ inputs.image_suffix ]]"
  variables:
    SEARCH_MAX_DEPTH: 20
  rules:
    - if: '"$[[ inputs.excluded_analyzers ]]" =~ /semgrep/'
      when: never
    # In case gitlab-advanced-sast also runs, exclude files already scanned by gitlab-advanced-sast
    - if: '$CI_COMMIT_BRANCH &&
          $GITLAB_FEATURES =~ /\bsast_advanced\b/ &&
          "$[[ inputs.excluded_analyzers ]]" !~ /gitlab-advanced-sast/ &&
          "$[[ inputs.run_advanced_sast ]]" == "true"'
      variables:
        SAST_EXCLUDED_PATHS: "$DEFAULT_SAST_EXCLUDED_PATHS, **/*.py, **/*.go, **/*.java, **/*.js, **/*.jsx, **/*.ts, **/*.tsx, **/*.cjs, **/*.mjs, **/*.cs, **/*.rb"
      exists:
        - '**/*.c'
        - '**/*.cc'
        - '**/*.cpp'
        - '**/*.c++'
        - '**/*.cp'
        - '**/*.cxx'
        - '**/*.h'
        - '**/*.hpp'
        - '**/*.scala'
        - '**/*.sc'
        - '**/*.php'
        - '**/*.swift'
        - '**/*.m'
        - '**/*.kt'
        - '**/*.yml'
        - '**/*.yaml'
        - '**/*.properties'
    ## In case gitlab-advanced-sast already covers all the files that semgrep-sast would have scanned
    - if: '$CI_COMMIT_BRANCH &&
          $GITLAB_FEATURES =~ /\bsast_advanced\b/ &&
          "$[[ inputs.excluded_analyzers ]]" !~ /gitlab-advanced-sast/ &&
          "$[[ inputs.run_advanced_sast ]]" == "true"'
      when: never
    - if: $CI_COMMIT_BRANCH
      exists:
        - '**/*.py'
        - '**/*.js'
        - '**/*.jsx'
        - '**/*.ts'
        - '**/*.tsx'
        - '**/*.c'
        - '**/*.cc'
        - '**/*.cpp'
        - '**/*.c++'
        - '**/*.cp'
        - '**/*.cxx'
        - '**/*.h'
        - '**/*.hpp'
        - '**/*.go'
        - '**/*.java'
        - '**/*.cs'
        - '**/*.scala'
        - '**/*.sc'
        - '**/*.php'
        - '**/*.swift'
        - '**/*.m'
        - '**/*.rb'
        - '**/*.kt'
        - '**/*.cjs'
        - '**/*.mjs'
        - '**/*.yml'
        - '**/*.yaml'
        - '**/*.properties'
        
sobelow-sast:
  extends: .sast-analyzer
  image:
    name: "$[[ inputs.image_prefix ]]/sobelow:$[[ inputs.image_tag ]]"
  rules:
    - if: '"$[[ inputs.excluded_analyzers ]]" =~ /sobelow/'
      when: never
    - if: $CI_COMMIT_BRANCH
      exists:
        - '**/mix.exs'

spotbugs-sast:
  extends: .sast-analyzer
  image:
    name: "$[[ inputs.image_prefix ]]/spotbugs:$[[ inputs.image_tag ]]"
  rules:
    - if: '"$[[ inputs.excluded_analyzers ]]" =~ /spotbugs/'
      when: never
    - if: '"$[[ inputs.include_experimental ]]" == "true"'
      exists:
        - '**/AndroidManifest.xml'
      when: never
    - if: $CI_COMMIT_BRANCH
      exists:
        - '**/*.groovy'
